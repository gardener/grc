package grc

import (
	"github.com/diamondburned/arikawa/v3/gateway"
	"github.com/diamondburned/arikawa/v3/state"
)

// Context contains some of the primary context variables
type Context struct {
	Mes *gateway.MessageCreateEvent
	Ses *state.State
	Han *Handler
}
