package grc

import "codeberg.org/gardener/arguments"

type Flag struct {
	Set    bool
	Raw    string
	Parsed any
}

type Flags struct {
	Flags map[string]Flag
}

type Arguments struct {
}

type Parser interface {
	// is given a set of arguments and general context
	// (depending on how its called it may)
	ParseArgument(args arguments.Arguments, ctx Context) (num int, ok bool, err error)
}

type parser struct {
}
