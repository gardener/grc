package grc

import "codeberg.org/gardener/arguments"

// NewCommandList returns a new blank command list
func NewCommandList() *CommandList {
	cl := CommandList{
		Map: make(map[string]*Command),
	}
	return &cl
}

// NewCommandListFrom creates a new command list from a list of commands
func NewCommandListFrom(cmds []*Command) *CommandList {
	cl := CommandList{
		Map: make(map[string]*Command, len(cmds)),
	}

	cl.AddBulk(cmds)

	return &cl
}

// CommandArgument details an argument that exists inside the command path
type CommandArgument struct {
	Commands *CommandList
	Name     string // in case there are multiple this allows identifying them

	RequireCommand bool // require being followed by a command

	// an optional validation function, if it matches it should return
	Validate func(s arguments.Arguments) (arguments.Arguments, bool, error)
}

// CommandList contains a list of commands
// and a bunch of methods to handle command routing
type CommandList struct {
	Map map[string]*Command

	// Picks the first one who's validation function matches
	Key []CommandArgument
}

// Add a command, and optionally a list of alternative names
func (cl *CommandList) Add(cmd *Command, aliases ...string) *CommandList {
	cl.Map[cmd.Name] = cmd // looks stupid but it works

	if len(aliases) == 0 {
		return cl
	}

	for _, a := range aliases {
		cl.Map[a] = AliasRelative(a, cmd.Name)
	}

	return cl
}

// AddBulk adds a large list of commands
func (cl *CommandList) AddBulk(cmds []*Command) *CommandList {
	for _, cmd := range cmds {
		cl.Add(cmd)
	}

	return cl
}

// AddAliasDirect adds a direct alias
func (cl *CommandList) AddAliasDirect(name string, to *Command) *CommandList {
	return cl.Add(&Command{
		Name:        name,
		DirectAlias: to,
	})
}

// AddAliasPath adds a path alias
func (cl *CommandList) AddAliasPath(name string, to string) *CommandList {
	return cl.Add(&Command{
		Name:      name,
		PathAlias: to,
	})
}

// AddAliasRelative adds a relative path alias
func (cl *CommandList) AddAliasRelative(name string, to string) *CommandList {
	return cl.Add(&Command{
		Name:          name,
		PathAlias:     to,
		RelativeAlias: true,
	})
}

// Traverse calls fn on each command and subcommand in this command map
// listAll controls if it includes commands which are not designated as public or not
// maxDepth controls how deep into subcommands it will go, 1 for no subcommands -1 for infinity
func (cl *CommandList) Traverse(doPrivate bool, maxDepth int, fn func(layer, index, length int, cmd *Command) error) error {
	return cl.traverse(doPrivate, maxDepth, fn, 0)
}

func (cl *CommandList) traverse(doAll bool, maxDepth int, fn func(layer, index, length int, cmd *Command) error, layer int) error {
	// if maxDepth is 0 then we are as low as we go
	if maxDepth == 0 {
		return nil
	}

	var i = 0
	for _, cmd := range cl.Map {
		i++ // we don't get a numerical index being a map, so we do it the old fashioned way

		if !doAll && !cmd.Public {
			continue
		}

		err := fn(layer, i, len(cl.Map), cmd)
		if err != nil {
			return err
		}

		// Traverse the subcommands
		err = cmd.Subcommands.traverse(doAll, maxDepth-1, fn, layer+1)
		if err != nil {
			return err
		}
	}
	return nil
}
