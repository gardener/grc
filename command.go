package grc

func Alias(name string, to string) *Command {
	return &Command{
		Name:      name,
		PathAlias: to,
	}
}

func AliasRelative(name string, to string) *Command {
	return &Command{
		Name:          name,
		PathAlias:     to,
		RelativeAlias: true,
	}
}

type Command struct {
	Name    string // the primary "name" of a command.
	Summery string // a short description of what a command does

	// Whether or not the command should be listed in the command list
	Public bool

	// Whether or not bots can run this command
	UsableBy UserTypes
	// Whether or not the command should be DMs or Guild Only
	UsableIn CommandLocation

	Subcommands CommandList

	// Exec is the function containing logic for a command
	// a null exec means this is a meta command for the router
	Exec func(*Context) error

	// Meta command Options
	// the following options are mutually exclusive with Exec
	// ( and usually mutually exclusive with each other )

	// PathAlias reroutes the command path to replace the current path to another
	// ex: the command `red green` PathAliases to `orange`
	// so the command `red green argument` gets rewritten to `orange argument`
	PathAlias string

	// RelativeAlias changes the behavior of PathAlias to not be anchored to the root
	// using the same example as above, but with RelativeAlias enabled
	// the command `red green argument` gets rewritten to `red orange argument`
	RelativeAlias bool

	// DirectAlias is a simple form of creating an alias
	// where instead of changing the path, it just points directly to another command
	DirectAlias *Command

	// wanna go nuts and do some weird ol things? go for it, go nuts
	// FuncAlias func(ctx FetcherContext) FetcherContext
}

// CommandConfig contains various bits of optional information about a command
type CommandConfig struct {
	//	// boolean flags are used for toggles are parsed in advance and they only take up the flag itself
	//	// bool flags are configured with a default state, if used in a command it will be the inverse of the default
	//	// however you may also specify the value when calling them via doing -b=true or -bol:true (anything after = or : must be valid to be parsed by strconv.ParseBool() )
	//	BoolFlags map[string]bool
	//
	//	// Data flags function more like arguments, they are read and removed from the arguments on parsetime and can be found in context.Flags
	//	// however they are not typed and must be parsed in command time similar to arguments, like with boolflags you can give it a default to use if it is not added into a command
	//	DataFlags map[string]string
	//
	//	// The minimum number of arguments that must be passed in order for the command to be executed
	//	MinimumArgs int
}

// UserTypes is a setting type for configuring whether or not bots should be able to run commands
type UserTypes uint8

// The various states of who can run commands
const (
	UserTypeUndefined = 0 // when unset it'll set itself to the default

	UserTypeMembers  UserTypes = 1 << iota // Members
	_                                      // UserTypeAllowedBots // currently not implemented TODO
	UserTypeAllBots                        // Any Bots
	UserTypeWebhooks                       // Webhooks

	UserTypeDefault = UserTypeMembers // The default
)

// UserType contains the same information as the UserType constants
var UserType = struct{ Member, AllBots, Webhooks UserTypes }{
	UserTypeMembers,
	UserTypeAllBots,
	UserTypeWebhooks,
}

// CommandLocation is a setting for if commands should be able to be used in DMs/Guilds
type CommandLocation uint8

// CommandLocation settings
const (
	CommandLocationUndefined = 0 // auto sets it to the default if undefined

	CommandLocationDMs       = 1 << iota // DMs
	CommandLocationGuilds                // In any guilds the bot joins
	CommandLocationAllowList             // In specifically allowed guilds

	// Allow in both DMs and guilds, Default
	CommandLocationDefault = CommandLocationDMs | CommandLocationGuilds
)
