package grc

import (
	"context"
	"strings"

	"codeberg.org/gardener/arguments"
	"github.com/diamondburned/arikawa/v3/gateway"
	"github.com/diamondburned/arikawa/v3/state"
)

// Get searches for a command with a much more user friendly interface
func (c *CommandList) Get(h *Handler, arg arguments.Arguments) (FetcherContext, error) {
	return c.Search(FetcherContext{
		Han:           h,
		caseSensitive: false,

		Args: arg,
	})
}

type FetcherContext struct {
	// idk, just in case, because some potentially wild things could happen
	ctx           context.Context
	caseSensitive bool

	Args arguments.Arguments

	Han *Handler
	Ses *state.State
	Mes *gateway.MessageCreateEvent

	Path []string          // The path to the command
	Keys map[string]string // Arguments that act as a part of the command path, eg cmd argument subcommand

	Command *Command
}

// Search attempts to find a command given the set of information
func (c CommandList) Search(ctx FetcherContext) (FetcherContext, error) {
	// if there are no remaining arguments or there are no commands just return here
	if ctx.Args.Len() == 0 || (len(c.Map) == 0 && c.Key != nil) {
		return ctx, nil
	}

	// Grab next argument key
	arg, _ := ctx.Args.Next() // we can ignore
	argText := arg.Content
	if !ctx.caseSensitive {
		argText = strings.ToLower(argText)
	}

	// If there isn't a command or its escaped/quoted then we can trail back now
	if arg.Escaped || arg.Quoted || c.Map[argText] == nil {
		prectx := ctx

		// But check if there is an argument key here first
		for _, x := range c.Key {
			args, ok, err := x.Validate(ctx.Args)
			if err != nil {
				return ctx, err
			}
			if !ok {
				continue
			}
			ctx.Args = args
			ctx.Path = append(ctx.Path, argText)
			ctx.Keys[x.Name]

			// check for subcommands in this route
			nctx, err := x.Commands.Search(ctx)
			if err != nil {
				return ctx, err
			}

			if nctx.Command == ctx.Command && x.RequireCommand {
				ctx = prectx
			} else {
				ctx = nctx
			}
		}

		return ctx, nil
	}

	cmd := c.Map[part]

	if cmd.Subcommands.Map != nil {
		nctx, err := cmd.Subcommands.Search(ctx)
		if nctx.Args.Cursor != ctx.Args.Cursor || err != nil {
			return nctx, err
		}
	}

	alc, ntx, key := c.CheckAlias(h, cmd, data.Args, caseSensitive)
	if alc != nil {
		cmd = alc
	}
	if ntx != "" {
		data.Args = ntx
	}
	if key != nil {
		for i, x := range key {
			data.Keys[i] = x
		}
	}

	return cmd, data
}

// CheckAlias checks alias information
func (c CommandList) CheckAlias(ctx FetcherContext) (Command, string, map[string]string) {
	if ali, ok := cmd.(CommandAlias); ok {
		c, newtxt := ali.Alias(h)
		if c == nil {
			return nil, "", nil
		}
		if sub, ok := c.(CommandSub); ok {
			sc, data := sub.Commands().Search(h, FetcherData{Args: newtxt}, caseSensitive)
			if sc != nil {
				return sc, data.Args, data.Keys
			}
		}
		return c, newtxt, nil
	}
	return nil, "", nil
}
